# Library for DataTourisme62

This PHP library can be used in your projects to connect them to the [DataTourisme62 API](https://tourisme62.opendatasoft.com/explore/?sort=modified).

## Requirements 

* PHP 5.6+

## Installation 

```
composer require inopale/lib-datatourisme62
```

## Testsuite

```
vendor/bin/phpunit -c phpunix.xml.dist
```

## Credits

© Inopale web
