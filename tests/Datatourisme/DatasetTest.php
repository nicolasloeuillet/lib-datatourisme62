<?php

namespace Tests\Datatourisme;

use Datatourisme\Dataset;
use PHPUnit\Framework\TestCase;
use Symfony\Component\VarDumper\VarDumper;

class DatasetTest extends TestCase
{
    /*public function testFindAll()
    {
        $a = true;
        $this->assertTrue($a);

        $dataset = new Dataset();
        $dataset->findAll();
    }*/

    /*public function testFindByLanguage()
    {
        $dataset = new Dataset();
        $dataset->findByLanguage('fr');
    }*/

    public function testFindById()
    {
        $dataset = new Dataset();
        $response = $dataset->findById('cdt62_manifestations');
        $this->assertContains("Manifestations dans le Pas-de-Calais", $response['metas']['description']);

        //$response = $dataset->findById('fake_dataset');
        //VarDumper::dump($response);
    }
}