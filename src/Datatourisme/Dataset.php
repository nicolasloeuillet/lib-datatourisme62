<?php

namespace Datatourisme;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\VarDumper\VarDumper;

class Dataset
{
    private $url = 'https://tourisme62.opendatasoft.com/api/';
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->url,
        ]);
    }

    /**
     * Returns all the datasets.
     *
     * @return array
     */
    public function findAll()
    {
        $response = $this->client->request('GET', 'datasets/1.0/search/');
        $body = json_decode($response->getBody(), true);

        return $body['datasets'];
    }

    /**
     * Returns all the datasets for a specific language.
     *
     * @param string $language
     *
     * @return array
     */
    public function findByLanguage($language = 'fr')
    {
        $response = $this->client->request('GET', 'datasets/1.0/search/', [
            'query' => ['refine.language' => $language],
        ]);
        $body = json_decode($response->getBody(), true);

        return $body['datasets'];
    }

    public function findById($datasetId)
    {
        try {
            $response = $this->client->request('GET', 'datasets/1.0/' . $datasetId);
        } catch (RequestException $e) {

        }

        $body = json_decode($response->getBody(), true);

        return $body;
    }
}
